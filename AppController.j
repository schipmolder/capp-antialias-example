/*
 * AppController.j
 * drawrect
 *
 * Created by You on March 30, 2012.
 * Copyright 2012, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>


@implementation AppController : CPObject
{
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    var theWindow = [[CPWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask],
        contentView = [theWindow contentView];

	var anotherView = [[DrawView alloc] initWithFrame:CGRectMake(0,0,100,100)];
	[contentView addSubview:anotherView];

    var label = [[CPTextField alloc] initWithFrame:CGRectMakeZero()];
    [label setStringValue:@"Hello World!"];
	[label setFont:[CPFont fontWithName:"HelveticaNeueLTProUltLt" size:"24"]];	// custom (thin) font defined in CSS - it's easier to see the antialiassing issue with a thin font
    [label sizeToFit];

    [label setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
    [label setCenter:[contentView center]];

    [contentView addSubview:label];

    [theWindow orderFront:self];

    // Uncomment the following line to turn on the standard menu bar.
    //[CPMenu setMenuBarVisible:YES];
}


@end


@implementation DrawView : CPView {


}

- (void)drawRect:(CGRect) aRect {
}

@end